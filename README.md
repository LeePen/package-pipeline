Jenkins script (Jenkinsfile) for  official Devuan package building jobs
=======================================================================

The Jenkinsfile in this repository is used by Devuan's [jenkins
instance](https://ci.devuan.org) to build and deploy the official Devuan
packages.

Note that [jenkins](https://ci.devuan.org) sources the Jenkinsfile directly for
each build so any changes here will be used for any subsequent builds that are
triggered.

The significant branches are:-

## Deployment branch

Used by the [production
builder](https://jenkins.devuan.dev/job/devuan-package-builder/) to build
official packages.

## Testing branch

Used in the [development and testing
builder](https://ci.devuan.org/job/test-pipeline) which builds in the same way
but does not notify [#devuan-ci](irc://libera.chat/devuan-ci) or deploy
artifacts to DAK.

## Master branch

The default branch.
